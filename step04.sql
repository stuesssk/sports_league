DROP TABLE IF EXISTS gamesplayed;

/*Creates Table fro Games Played*/
CREATE TABLE gamesplayed(
    GameID      INTEGER(4) NOT NULL PRIMARY KEY,
    Day         DATE NOT NULL,
    StartTime   INTEGER(4) NOT NULL,
    HomeID      INTEGER(4) NOT NULL,
    VisitorID   INTEGER(4) NOT NULL
);
