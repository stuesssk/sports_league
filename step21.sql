/*
    Creates a relational view calculates the count of the wins for
    each team for the entire season
*/
CREATE VIEW wincount AS
SELECT name, count(gameid) AS wins
    FROM teams, gamestats
    WHERE result = 'Win' and teams.teamid = gamestats.teamid
    GROUP BY gamestats.teamid;

/*
    Creates a relational view calculates the count of the losses for
    each team for the entire season
*/
CREATE VIEW losecount AS
SELECT name, count(gameid) AS lose
    FROM teams, gamestats
    WHERE result = 'Lose' and teams.teamid = gamestats.teamid
    GROUP BY gamestats.teamid;

/*Dsipalys each teams win-loss record*/
SELECT wincount.name, wincount.wins, losecount.lose
    FROM wincount, losecount
    WHERE wincount.name = losecount.name;
    
/*No need to keep the relational views around, so drop them*/
DROP VIEW wincount;
DROP VIEW losecount;
