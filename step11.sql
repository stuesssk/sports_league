/*
    Calculates the sum of each teams points scored this season and 
    orders from highest to lowest but limits to the first or the highest
    value
*/
SELECT Name AS 'Team With Highest Cumulative Score',
       SUM(score) AS 'Total Score'
    FROM teams, gamestats
    WHERE teams.teamid = gamestats.teamid
    GROUP BY teams.teamid
    ORDER BY SUM(score) DESC LIMIT 01;
