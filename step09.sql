/* 
    Given a teamid lists the players name and jersery number
    ordered by jersey number.
*/
SELECT CONCAT(firstname, ' ', lastname) AS Player, jerseynum AS Jersey
    From players
    WHERE teamid = 1
    ORDER BY jerseynum;
