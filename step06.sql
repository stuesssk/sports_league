/*Fill Data for Teams*/
DELETE FROM teams WHERE TeamID IS NOT NULL;

/*
    Columns:
    (TeamID, Name)
*/
INSERT INTO teams VALUES
(1, 'Cincinnati Reds'),
(2, 'Chicago Cubs'),
(3, 'Milwaukee Brewers'),
(4, 'Pittsburgh Pirates'),
(5, 'St. Louis Cardinals');

/*
    Fill Data for Players
    Players 1-7 are on Cincinnati Reds
    Players 8-13 are on Chicago Cubs
    players 14-19 are on Milwaukee Brewers
    players 20-25 are on Pittsburgh Pirates
    players 25-31 are on St. Louis Cardinals
    players 32-35 are free agents
    Columns are:
    (PlayerId, First name, Last Name, TeamID, Jersey Number)
*/
DELETE FROM players WHERE PlayerID IS NOT NULL;
INSERT INTO players VALUES
(1, 'Joey', 'Votto', 1, 19),
(2, 'Zack', 'Cozart', 1, 2),
(3, 'José', 'Peraza', 1, 9),
(4, 'Scott', 'Schebler', 1, 43),
(5, 'Adam', 'Duvall', 1, 23),
(6, 'Eugenio', 'Suarez', 1, 7),
(7, 'Tucker', 'Barnhart', 1, 16),
(8, 'Kris', 'Bryant', 2, 17),
(9, 'Javier', 'Báez', 2, 9),
(10, 'Albert', 'Almora Jr.', 2, 5),
(11, 'Jason', 'Hayward', 2, 22),
(12, 'Willson', 'Contreras', 2, 40),
(13, 'Anthony', 'Rizzo', 2, 44),
(14, 'Manny', 'Piña', 3, 9),
(15, 'Travis', 'Shaw', 3, 21),
(16, 'Jesús', 'Aguilar', 3, 24),
(17, 'Eric', 'Thames', 3, 7),
(18, 'Jett', 'Bandy', 3, 47),
(19, 'Domingo', 'Santana', 3, 16),
(20, 'Adam', 'Frazier', 4, 26),
(21, 'Josh', 'Harrison', 4, 5),
(22, 'Gregory', 'Polanco', 4, 25),
(23, 'Jordy', 'Mercer', 4, 10),
(24, 'David', 'Freese', 4, 23),
(25, 'Fancisco', 'Cervelli', 4, 29),
(26, 'Tommy', 'Pham', 5, 28),
(27, 'Jed', 'Gyorko', 5, 3),
(28, 'Matt', 'Adams', 5, 18),
(29, 'Jose', 'Martinez', 5, 58),
(30, 'Kolten', 'Wong', 5, 16),
(31, 'Yadier', 'Molina', 5, 4),
(32, 'Steve', 'Smith', NULL, NULL),
(33, 'James', 'Jones', NULL, NULL),
(34, 'Sean', 'Stuessel', NULL, NULL),
(35, 'Adam', 'Scott', NULL, NULL);

/*
    Fill Data for Player Stats
    Columns are:
    (PlayerID, Age, Weight, Height, Postion, Games Played, At Bats, 
                                                           Runs, Hits)
*/
DELETE FROM playerstats WHERE PlayerID IS NOT NULL;
INSERT INTO playerstats VALUES
(1, 35, 190, 73, '1B', 52, 184, 39, 52),
(2, 29, 210, 69, 'SS', 45, 167, 30, 50),
(3, 30, 215, 71, '2B', 49, 193, 24, 51),
(4, 32, 212, 68, 'RF', 50, 181, 24, 45),
(5, 25, 211, 67, 'LF', 51, 199, 30, 54),
(6, 24, 207, 66, '3B', 51, 184, 30, 53),
(7, 28, 210, 71, 'C', 36, 115, 19, 31),
(8, 27, 208, 73, '3B', 49, 187, 34, 54),
(9, 26, 213, 69, '2B', 45, 145, 21, 38),
(10, 24, 210, 68, 'CF', 42, 101, 15, 26),
(11, 23, 208, 68, 'RF', 39, 140, 17, 36),
(12, 22, 204, 67, 'C', 45, 141, 16, 35),
(13, 24, 194, 66, '1B', 51, 194, 28, 44),
(14, 22, 196, 67, 'C', 30, 99, 15, 30),
(15, 25, 197, 69, '3B', 47, 189, 25, 36),
(16, 27, 298, 68, '1B', 49, 94, 13, 37),
(17, 23, 192, 66, '1B', 47, 161, 42, 46),
(18, 31, 202, 69, 'C', 31, 104, 42, 46),
(19, 27, 211, 72, 'RF', 49, 161, 21, 43),
(20, 21, 194, 70, '2B', 34, 119, 16, 37),
(21, 24, 199, 71, '2B', 52, 197, 21, 58),
(22, 29, 210, 74, 'LF', 39, 135, 18, 37),
(23, 28, 208, 75, 'SS', 52, 179, 21, 45),
(24, 24, 202, 73, '3B', 36, 112, 12, 28),
(25, 24, 201, 70, 'C', 45, 147, 19, 36),
(26, 22, 190, 70, 'CF', 22, 75, 15, 24),
(27, 23, 180, 68, '3B', 43, 155, 21, 48),
(28, 22, 185, 67, 'LF', 31, 48, 4, 14),
(29, 30, 180, 70, '1B', 23, 56, 8, 16),
(30, 31, 182, 67, '2B', 41, 133, 18, 37),
(31, 29, 184, 68, 'C', 44, 170, 17, 44),
(32, 22, 180, 75, 'P', NULL, NULL, NULL, NULL),
(33, 25, 182, 73, NULL, NULL, NULL, NULL, NULL),
(34, 30, 180, 69, 'SS', NULL, NULL, NULL, NULL),
(35, 31, 176, 68, NULL, NULL, NULL, NULL, NULL);

/*
    Fill Data for Games Played
    Columns Are:
    (GameID, Date, Time, HomeID, VisitorID)
*/

DELETE FROM gamesplayed WHERE GameID IS NOT NULL;

INSERT INTO gamesplayed VALUES
(1, '2017-6-1', 1700, 1, 2),
(2, '2017-6-1', 1750, 3, 4),
(3, '2017-5-28', 1200, 5, 1),
(4, '2017-5-28', 1800, 5, 1),
(5, '2017-5-28', 1230, 4, 2),
(6, '2017-5-27', 1200, 1, 3),
(7, '2017-5-27', 1300, 4, 2),
(8, '2017-5-26', 1840, 3, 5),
(9, '2017-5-26', 1700, 2, 1),
(10, '2017-5-25', 1800, 1, 4),
(11, '2017-5-25', 1700, 3, 5),
(12, '2017-5-23', 1750, 2, 4);


/*
    Fill Data for Games Stats
    Columns Are:
    (RecordID, GameID, TeamID, score, result)
*/
    
DELETE FROM gamestats WHERE RecordID IS NOT NULL;
INSERT INTO gamestats VALUES
(1, 1, 1, 10,'Win'),
(2, 1, 2, 6,'Lose'),
(3, 2, 3, 4,'Lose'),
(4, 2, 4, 8,'Win'),
(5, 3, 5, 4,'Lose'),
(6, 3, 1, 6,'Win'),
(7, 4, 5, 7,'Win'),
(8, 4, 1, 3,'Lose'),
(9, 5, 4, 5,'Win'),
(10, 5, 2, 3,'Lose'),
(11, 6, 1, 4,'Win'),
(12, 6, 3, 2,'Lose'),
(13, 7, 4, 0,'Lose'),
(14, 7, 2, 3,'Win'),
(15, 8, 3, 11,'Win'),
(16, 8, 5, 2,'Lose'),
(17, 9, 2, 0,'Lose'),
(18, 9, 1, 4,'Win'),
(19, 10, 1, 5,'Win'),
(20, 10, 4, 0,'Lose'),
(21, 11, 5, 2,'Win'),
(22, 11, 3, 1,'Lose'),
(23, 12, 2, 2,'Lose'),
(24, 12, 4, 3,'Win');
