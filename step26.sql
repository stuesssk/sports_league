/*Output teams table into teams.txt file*/
SELECT * FROM teams 
    INTO OUTFILE 'teams.txt'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';

/*Output players table into players.txt file*/
SELECT * FROM players 
    INTO OUTFILE 'players.txt'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';

/*Output playerstats table into playerstats.txt file*/
SELECT * FROM playerstats 
    INTO OUTFILE 'playerstats.txt'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';


/*Output gamestats table into gamestats.txt file*/
SELECT * FROM gamestats 
    INTO OUTFILE 'gamestats.txt'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';

/*Output gamesplayed table into gamesplayed.txt file*/
SELECT * FROM gamesplayed 
    INTO OUTFILE 'gamesplayed.txt'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n';
