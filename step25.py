#!/usr/bin/env python3
def main():
    # Table Names list to test user input against
    table_names = ['teams','players','playerstats','gamestats','gamesplayed']
    inputstring = None

    # Open the file for writing, will erase anything already in the file
    filehandle = open("step25.sql", "w")

    # Ask for a table name
    print("Input table name from the list:")
    print(table_names)

    while inputstring not in table_names:
        # Get user input
        inputstring = input("Table:")
        # Ensuring user input is the same case as our list of table names
        inputstring = inputstring.lower()

        # Break if vaild table name, if not ask again
        if inputstring in table_names:
            break
        print("Please Enter a Valid Table")

    # Craft SQL select statement to select all records from the given table
    outputstring = "SELECT * FROM " + inputstring
    # Write the select querry to the file
    filehandle.write(outputstring)
    # Don't forget to close the file
    filehandle.close()

if __name__ == "__main__": main()
