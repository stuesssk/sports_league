/*
    Calculates the sum of scored in each game this season and 
    orders from highest to lowest but limits to the first or the highest
    value
*/
SELECT gameid AS 'Game ID', sum(score) AS 'Highest Points In a Game'
    FROM gamestats
    GROUP BY gameid
    ORDER BY sum(score) DESC LIMIT 01;
