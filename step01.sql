DROP TABLE IF EXISTS teams;

/*Creates Table for teams*/
CREATE TABLE teams(
    TeamID  INTEGER(4) NOT NULL PRIMARY KEY,
    Name    VARCHAR(32) NOT NULL
);
