/* 
    Prints list of all players with output in for of "lastname, firstname
    with field header of Name, in alphabeticale order by lastname.
*/
SELECT CONCAT(lastname , ', ', firstname) AS Name, Age, Height, Weight
    FROM players, playerstats
    WHERE players.playerid = playerstats.playerid
    ORDER BY lastname;
