DROP TABLE IF EXISTS players;

/*Creates Table for players*/
CREATE TABLE players(
    PlayerID    INTEGER(4) NOT NULL PRIMARY KEY,
    FirstName   VARCHAR(32) NOT NULL,
    LastName    VARCHAR(32) NOT NULL,
    TeamID      INTEGER(4),
    JerseyNum   INTEGER(2),
    CONSTRAINT team_key
        FOREIGN KEY (TeamID)
        REFERENCES teams (TeamID)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
);
