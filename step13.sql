/*Lists players not assigned to a team*/
SELECT CONCAT(lastname, ", ", firstname) AS 'Free Agents'
    FROM players
    WHERE teamid IS NULL;
