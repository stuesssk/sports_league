/*Calculates total number of teams in the league*/
SELECT count(teamid) AS 'Number of Teams'
    FROM teams;
