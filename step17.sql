/*Calculates the tallest player(s) in the league*/
SELECT CONCAT(firstname, ' ', lastname) AS 'Tallest Player(s)', height AS 'Height (in)'
    FROM players, playerstats
    WHERE height = (SELECT max(height) FROM playerstats)
    AND players.playerid = playerstats.playerid;
