/*
    Prints a report that shows the final score of each game
    with the names of the home and visiting team.
*/
SELECT hometeam.name AS 'Home Team', visitteam.name AS 'Visiting Team', 
        CONCAT(visit.score,"-", home.score) AS Score
    FROM gamestats AS home, gamestats AS visit, gamesplayed, 
         teams as hometeam, teams as visitteam
    WHERE home.gameid = visit.gameid
    AND home.gameid = gamesplayed.gameid
    AND home.teamid = gamesplayed.homeid
    AND visit.teamid = gamesplayed.visitorid
    AND hometeam.teamid = home.teamid
    AND visitteam.teamid = visit.teamid;

