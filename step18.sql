/*
    Prints a report showing al games and dates and includes
    home and visitor team names
*/
SELECT Day, StartTime AS 'Start Time', home.name 
       AS 'Home Team', visitor.name AS 'Visiting Team'
    FROM gamesplayed, teams AS home, teams AS visitor
    WHERE home.teamid = gamesplayed.homeid
       and visitor.teamid = gamesplayed.visitorid
    
