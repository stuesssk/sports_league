DROP TABLE IF EXISTS playerstats;

/*Creates Table for player statistics*/
CREATE TABLE playerstats(
    PlayerID    INTEGER(2) NOT NULL PRIMARY KEY,
    Age         INTEGER(2) NOT NULL,
    Weight      INTEGER(3) NOT NULL,
    Height      INTEGER(3) NOT NULL,
    Position    ENUM('P', 'C', '1B', '2B', 'SS', '3B', 'LF', 'CF', 'RF'),
    GamesPlayed INTEGER(3),
    AtBats      INTEGER(4),
    Runs        INTEGER(4),
    Hits        INTEGER(4),
    CONSTRAINT player_key
    FOREIGN KEY (PlayerID)
    REFERENCES players (PlayerID)
    ON UPDATE RESTRICT
    ON DELETE CASCADE
);
