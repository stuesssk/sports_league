/*Script to drop any and all tables created if they exist*/
DROP TABLE IF EXISTS gamestats;
DROP TABLE IF EXISTS gamesplayed;
DROP TABLE IF EXISTS playerstats;
DROP TABLE IF EXISTS players;
DROP TABLE IF EXISTS teams;
