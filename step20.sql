/*
    The relational view calculates the average score per game, each team
    had for the entire season
*/
CREATE VIEW gameavg AS
SELECT name, avg(score) AS scoreavg
    FROM teams, gamestats
    WHERE teams.teamid = gamestats.teamid
    GROUP BY  gamestats.teamid;

/*Calculates the team with the highest average score*/
SELECT Name, scoreavg AS 'Highest Avg Score Per Game'
    FROM gameavg
    WHERE scoreavg = (SELECT max(scoreavg) FROM gameavg);

/*No need to keep the relational view around, so drop it*/
DROP VIEW gameavg;
