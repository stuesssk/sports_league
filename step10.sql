/*Calculates the highest score for any team in a single game*/
SELECT Name AS 'Team With Highest Single Game Score', score AS 'Score' 
    FROM teams, gamestats
    WHERE teams.teamid = gamestats.teamid
    AND score = (SELECT max(score) FROM gamestats);
