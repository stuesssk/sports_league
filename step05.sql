DROP TABLE IF EXISTS gamestats;

/*Creates table for game statistics*/
CREATE TABLE gamestats(
    RecordID    INTEGER(4) NOT NULL PRIMARY KEY,
    GameID      INTEGER(4) NOT NULL,
    TeamID      INTEGER(4) NOT NULL,
    Score       INTEGER(3) NOT NULL,
    Result      ENUM('Win', 'Lose') NOT NULL,
    CONSTRAINT game_key
    FOREIGN KEY (GameID)
    REFERENCES gamesplayed (GameID)
    ON UPDATE RESTRICT
    ON DELETE CASCADE
);
