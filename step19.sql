/*
    The relational view counts the number of games each team has won
    for the entire season
*/
CREATE VIEW wincount AS
SELECT name, count(gameid) AS wins
    FROM teams, gamestats
    WHERE result = 'Win' and teams.teamid = gamestats.teamid
    GROUP BY gamestats.teamid;

/*Calculates the team with the most wins from the relational view*/
SELECT Name, Wins
    FROM wincount
    WHERE wins = (SELECT max(wins) FROM wincount);

/*No need to keep the relational view around, so drop it*/
DROP VIEW wincount;
