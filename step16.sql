/*Counts the number of games played on a specific day*/
SELECT count(gameid) AS 'Games Played On 2017-5-28'
    FROM gamesplayed
    WHERE day = '2017-5-28';
